# Bearcoda Front-end Coding Test

## Introduction

Welcome to the Bearcoda test. In this test, you will be working on a gallery design project. 

## Project Description

### Task Overview

Your task involves creating implementations of the provided gallery design:

1. **Basic HTML, CSS, and JS**: Create the gallery using vanilla HTML, CSS, and JavaScript.

2. **Framework-Based Implementations (Pick Two)**: Choose at least two out of the three front-end frameworks listed below and develop responsive versions of the gallery using the selected frameworks:

    - Vue.js
    - React
    - Angular

### Responsive Design

Ensure that your gallery behaves responsively on different screen sizes and devices. You should aim for a seamless user experience across various resolutions.

## Getting Started

1. Visit the Figma design for this project using the following link: [Gallery Design Figma](https://www.figma.com/file/sE8SlpQ2zwaNF6Wyz8Asey/UI-Test?type=design&node-id=1%3A448&t=GBlkuOzQJJJPwI1o-1).

2. The project assets are located in the "Assets" folder inside this repository.

## Submission Instructions

1. **Create a Branch**: Begin by creating a branch with your name as its identifier.

2. **Commit and Push**: As you work on your implementations, make regular commits to your branch. When you are ready to submit, ensure all your changes are committed, and then push the branch to this repository.

## Evaluation

Your submission will be evaluated based on the following criteria:

- Adherence to the provided design.
- Code quality and organization.
- Responsiveness and compatibility with various devices.
- Usage of appropriate libraries and frameworks (if applicable).
- Creativity and attention to detail.

Feel free to reach out in Slack if you have any questions or need clarification on any aspect of the test.

Best of luck, and we look forward to reviewing your work!